/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceService;

import datacore.Vehicle;
import java.util.HashSet;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class RequestsHolder {
    private static RequestsHolder holder;
    final private HashSet<Request> set;
    
    private RequestsHolder() {
        set = new HashSet<>();
    }
    
    public static RequestsHolder Init() {
        if(holder == null) {
            synchronized (RequestsHolder.class) {
                holder = new RequestsHolder();
            }
        }
        return holder;
    }
    
    public Request getToken(String requestNumber, Vehicle vehicle) {
        String id = vehicle.getVid() + requestNumber;
        
        for(Request request : set) {
            if(request.getRequestNumber().equals(id))
                return request;
        }
        return null;
    }
    
    public void add(String token, String requestNumber, Vehicle vehicle) {
        Request req = new Request(token, vehicle.getVid() + requestNumber);
        if(set.contains(req))
            set.remove(req);
        set.add(req);
    }
    
    public void remove(Request request) {
        set.remove(request);
    }
}
