/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceService;

import isr.clearanceLib.common.ClearingServiceFactory;
import isr.clearanceLib.common.ClearingService;
import datacore.Vehicle;
import isr.commonTools.Dictionary;
import isr.commonTools.StringEx;
import zlib.ZLog;
import isr.commonTools.Service;
import datacore.Organization;
import isr.clearanceLib.common.TerminalInfo;
import isrdata.CCProcess;
import isrdata.IsrEvents;
import isrdata.OrgClearance;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;
import zlib.ZDate;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class CSEvents extends IsrEvents{
    private final RequestsHolder holder;  
    private static final ZLog _logger = CSMain.getLogger();
    private static final CSMain main = CSMain.getInstance();
    private static final String PAYMENT = "PAYMENT";
    private static final String TOKEN = "TOKEN";
    private static final String ID = "ID";
    private static final String PH = "PH";
    private static final String NM = "NM";
    private static final String LP = "LP";
    private static final String CM = "CM";
    private static final String PN = "PN";
    private static final String FT = "FT";

    public CSEvents() {
        holder = RequestsHolder.Init();
    }
    
    @Override
    protected boolean onNewOrganization(Organization organization){  
        TerminalInfo info = main.getDataBase().getOrganizationInfo(organization.getObjectID());
        synchronized(main.getLocker()) {
            main.getOrganizationMap().put(organization.getObjectID(), info);
        }
        return true;
    }
    
    @Override
    protected boolean onPaymentRequest(Vehicle vehicle, String manOrSwipe, String drNum, 
            String cardNum, String expDate, String cvc, String sum, String type, 
            String pNum, String subPayload, String refNum) {

        String requestId = vehicle.getVid() + "-" + refNum;
        logEvent(PAYMENT, requestId, "1", "Fired.");
        String paramX = getParamX(vehicle.getVid());
        
        Organization org = vehicle.getOrganization();
        logEvent(PAYMENT, requestId, "1", String.format("Details: Org = %s, vehicle = %s, manOrSwipe = %s, drNum = %s, "
                + "cardNum = %s, expDate = %s, cvc = %s, sum = %s, type = %s, pNum = %s, "
                + "subPayload = %s, reqRefNum = %s, paramX = %s", vehicle.getOrganizationID(),
                vehicle.getVid(), manOrSwipe, drNum, cardNum, expDate, cvc, 
                sum, type, pNum, subPayload, refNum, paramX));
        
        Request request = holder.getToken(refNum, vehicle);
        Map<String, String> subPayLoadMap = Dictionary.getDictonary(subPayload, ";", ":");
        
        if(request != null) {
            logEvent(PAYMENT, requestId, "2", "Got token.");
            String id = subPayLoadMap.get(ID);
            if(!StringEx.isNullOrEmpty(id)) {
                id = StringEx.decrypt(id, request.getToken());
                subPayLoadMap.remove(ID);
            } else {
                id = StringEx.EMPTY;
            }
            subPayLoadMap.put(ID, id);
            
            try {
                TerminalInfo info = main.getOrganizationMap().get(org.getObjectID());
                
                logEvent(PAYMENT, requestId, "3", "Working with service " + info.getServiceName());
                ClearingService service = ClearingServiceFactory.get(info.getServiceName(), 
                        main.getOrganizationMap().get(org.getObjectID()),
                        vehicle.getVid(), manOrSwipe, drNum, cardNum, expDate, cvc, 
                        sum, type, pNum, subPayload, refNum, request.getToken(), id, paramX, 
                        CSMain.getSettings().getProperty("debug").equals("true"),
                        CSMain.getInstance().getRootFolder());
                service.commitPayment();
                CreditResponseType status = CreditResponseType.convert(service.getStatusCode());

                logEvent(PAYMENT, requestId, "4", 
                        String.format("Status code %s. converted to %s, last digits: %s, Transaction id: %s", 
                        service.getStatusCode(), status.toString(), service.getCardLastDigits(), service.getAuthNumebr()));
                
                replyPaymentToVehicle(vehicle, service.getAuthNumebr(), 
                        service.getCardLastDigits(), status, refNum);
                logEvent(PAYMENT, requestId, "5", "Replayed to vehicle.");
                storeTransaction(vehicle, drNum, refNum, manOrSwipe, type, 
                        service.getCardLastDigits(), service.getCardBrand(), 
                        sum, pNum, service.getAuthNumebr(), service.getStatusCode(), 
                        subPayLoadMap, paramX);   
                logEvent(PAYMENT, requestId, "6", "Done.");
                
            } catch (Exception ex) {
                logEvent(PAYMENT, requestId, "0", "Exception.", ex);
                replyPaymentToVehicle(vehicle, StringEx.EMPTY, StringEx.EMPTY, CreditResponseType.Other, refNum);
                storeTransaction(vehicle, drNum, refNum, manOrSwipe, type, StringEx.EMPTY, "0", sum,
                    pNum, StringEx.EMPTY, "999", subPayLoadMap, paramX);
            }
            holder.remove(request); 
 
        } else {
            logEvent(PAYMENT, requestId, "2", "No valid token.");
            replyPaymentToVehicle(vehicle, StringEx.EMPTY, StringEx.EMPTY, CreditResponseType.Other, refNum);
            storeTransaction(vehicle, drNum, refNum, manOrSwipe, type, StringEx.EMPTY, "0", sum, 
                       pNum, StringEx.EMPTY, "999", subPayLoadMap, paramX);
        }
        return true;
    }
    
    private void storeTransaction(Vehicle vehicle, String drNum, String refNum, String manOrSwipe, 
            String operationType, String cardLastDigits, String cardBranch, String sum, String payments,
            String autorizationRef, String statusCode, Map<String, String> subPayLoadMap, String paramX) {

        main.getDataBase().storeTransaction(new Transaction(vehicle, drNum, refNum, ZDate.now(), manOrSwipe, operationType, cardLastDigits, cardBranch, 
                Float.parseFloat(sum), Integer.parseInt(payments), autorizationRef, statusCode, 
                subPayLoadMap.get(NM), subPayLoadMap.get(PH), StringEx.EMPTY,  StringEx.EMPTY, 
                StringEx.EMPTY, subPayLoadMap.get(LP), subPayLoadMap.get(ID), subPayLoadMap.get(CM), 
                subPayLoadMap.get(PN), subPayLoadMap.get(FT), paramX));
    }
    
    private void replyPaymentToVehicle(Vehicle vehicle, String authNum, 
            String creditCardNum, CreditResponseType type, String refNum) {
        CCProcess cc = (CCProcess)vehicle.getChild(CCProcess.ObjectCode, null);
        cc.replyPayment(authNum, creditCardNum, type.getValue(), refNum);
        cc.commit();
    }
    
    @Override
    protected boolean onTokenReceived(Vehicle vehicle, String tVal, String reqRefNum){
        String requestId = vehicle.getVid() + "-" + reqRefNum;
        CCProcess cc = (CCProcess)vehicle.getChild(CCProcess.ObjectCode, null);
        logEvent(TOKEN, requestId, "1", "Fired");
        holder.add(tVal, reqRefNum, vehicle);
        logEvent(TOKEN, requestId, "1.1", "Token value - " + tVal);
        boolean ret = cc.confirmToken(Service.TokenService.getName(), reqRefNum);
        cc.commit();
        logEvent(TOKEN, requestId, "2", "Confirm");
        return true;
    }
    
    private static String getParamX(String vid) {
        String tmp = vid.replace("-", "");
        if(tmp.contains(" ")) {
            tmp = tmp.substring(0, tmp.indexOf(" "));
        }
        if(tmp.length() > 7) {
            tmp = tmp.substring(0, 6);
        }
        
        tmp += getTimeStampString();

        return tmp;
    }
    
    private static String getTimeStampString() {
        GregorianCalendar date = new GregorianCalendar();
        
        return String.format("%s%s%s%s%s%s", 
            getTimeString(date.get(Calendar.YEAR)),
            getTimeString(date.get(Calendar.MONTH)+1),
            getTimeString(date.get(Calendar.DAY_OF_MONTH)),
            getTimeString(date.get(Calendar.HOUR_OF_DAY)),
            getTimeString(date.get(Calendar.MINUTE)),
            getTimeString(date.get(Calendar.SECOND))
            );
    }
    
    private static String getTimeString(int value) {
        String tmp = Integer.toString(value);
        if(tmp.length() == 1) {
            tmp = "0" + tmp;
        }
        if(tmp.length() == 4) {
            tmp = tmp.substring(2);
        }
        return tmp;
    }
    
    public static void logEvent(String event, String requestId, String serial, String msg) {
        _logger.logEvent(String.format("%10s id-%30s \tStep#%s-%s", event, requestId, serial, msg));
    }
    
    public static void logEvent(String event, String requestId, String serial, String msg, Exception ex) {
        _logger.logEvent(String.format("%10s id-%30s \tStep#%s-%s", event, requestId, serial, msg));
        _logger.logException(ex);
    }

}
