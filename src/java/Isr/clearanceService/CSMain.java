package isr.clearanceService;

import isr.clearanceLib.common.TerminalInfo;
import isrdata.IsrEvents;
import isrdata.IsrMain;
import isrdata.IsrQueries;
import java.util.HashMap;

/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class CSMain extends IsrMain{

    private static final String APP_NAME = "PC";
    private static final String VERSION = "O1.4"; // Ver O1.4 - implementation of datacore Z4.03
    private static final String ROOT_DIRECTORY = "/isr/IsrCreditServer/";
    
    private final HashMap<String, TerminalInfo> organizations = new HashMap();
    private final Object locker = new Object();
    private DataBaseEx database;
     
    public CSMain() {
        super(ROOT_DIRECTORY, APP_NAME, VERSION);
    }
    
    public Object getLocker() {
        return locker;
    }

    public HashMap<String, TerminalInfo> getOrganizationMap() {
        return organizations;
    }
    @Override
    protected IsrEvents createEventHandler() {
        return new CSEvents();
    }
    
    public static CSMain getInstance() {
        return (CSMain)IsrMain.getInstance();
    }
    
    protected DataBaseEx getDataBase() {
        return database;
    }
    
    @Override
    public boolean init() {
        boolean flag;
        try {
            if (!super.init())
                return false;
            
            flag = IsrQueries.createChargeService(APP_NAME, null);
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        
        database = new DataBaseEx();
        database.init();
        return flag;
    }

    @Override
    protected String[] getProducedObjects() {
        return new String[] {};
    }

    @Override
    protected String[] getConsumedObjects() {
        return new String[] {};
    }
}
