package isr.clearanceService;

import datacore.Vehicle;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import zlib.ZDate;

/**
 *
 * @author Zvi Lifshitz <zvil@zvil.com>
 */
public class Transaction {
    private final Vehicle vehicle;
    private final String drNum;
    private final String refNum;
    private final ZDate transactionTime;
    private final String manOrSwipe;
    private final String operationType;
    private final String cardLastDigits;
    private final String cardBranch;
    private final float sum;
    private final int payments;
    private final String autorizationRef;
    private final String statusCode;
    private final String clientName;
    private final String clientPhone;
    private final String clientAddress;
    private final String clientCity;
    private final String clientZip;
    private final String clientInfo;
    private final String clientID;
    private final String clientCompanyID;
    private final String productInfo;
    private final String remarks;
    private final String paramX;

    public Transaction(Vehicle vehicle, String drNum, String refNum, ZDate transactionTime, String manOrSwipe, String operationType, String cardLastDigits, String cardBranch, float sum, int payments, String autorizationRef, String statusCode, String clientName, String clientPhone, String clientAddress, String clientCity, String clientZip, String clientInfo, String clientID, String clientCompanyID, String productInfo, String remarks, String paramX) {
        this.vehicle = vehicle;
        this.drNum = drNum;
        this.refNum = refNum;
        this.transactionTime = transactionTime;
        this.manOrSwipe = manOrSwipe;
        this.operationType = operationType;
        this.cardLastDigits = cardLastDigits;
        this.cardBranch = cardBranch;
        this.sum = sum;
        this.payments = payments;
        this.autorizationRef = autorizationRef;
        this.statusCode = statusCode;
        this.clientName = clientName;
        this.clientPhone = clientPhone;
        this.clientAddress = clientAddress;
        this.clientCity = clientCity;
        this.clientZip = clientZip;
        this.clientInfo = clientInfo;
        this.clientID = clientID;
        this.clientCompanyID = clientCompanyID;
        this.productInfo = productInfo;
        this.remarks = remarks;
        this.paramX = paramX;
    }

    /**
     * Fill an SQL statement with values from this object. The statement is
     *  "INSERT INTO transactions (Organization,VehicleID,DriverID,RefNum,TransactionDateTime,ManOrSwipe, " +
        "OperationType,CardLastDigits,CardBranch,Sum,Payments,AutorizationRef,StatusCode,ClientName,ClientPhone, " +
        "ClientAddress,ClientCity,ClientZip,ClientInfo,ClientID,ClientCompanyID,ProductInfo,Remarks) " +
        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
     * @param stmt
     * @throws SQLException 
     */
    void fillStatement(PreparedStatement stmt) throws SQLException {
        int i = 1;
	stmt.setString(i++, vehicle.getOrganizationID());
	stmt.setString(i++, vehicle.getVid());
	stmt.setString(i++, drNum);
	stmt.setString(i++, refNum);
	stmt.setTimestamp(i++, transactionTime.toSQLtime());
	stmt.setString(i++, manOrSwipe);
	stmt.setString(i++, operationType);
	stmt.setString(i++, cardLastDigits);
	stmt.setString(i++, cardBranch);
	stmt.setFloat(i++, sum);
	stmt.setInt(i++, payments);
	stmt.setString(i++, autorizationRef);
	stmt.setString(i++, statusCode);
	stmt.setString(i++, clientName);
	stmt.setString(i++, clientPhone);
	stmt.setString(i++, clientAddress);
	stmt.setString(i++, clientCity);
	stmt.setString(i++, clientZip);
	stmt.setString(i++, clientInfo);
	stmt.setString(i++, clientID);
	stmt.setString(i++, clientCompanyID);
	stmt.setString(i++, productInfo);
	stmt.setString(i++, remarks);
        stmt.setString(i++, paramX);
    }
}
