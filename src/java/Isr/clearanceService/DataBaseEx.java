package isr.clearanceService;

import isr.clearanceLib.common.TerminalInfo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import zlib.ZDataBase;
import zlib.ZLog;

/**
 *
 * @author Zvi Lifshitz <zvil@zvil.com>
 */
public class DataBaseEx extends ZDataBase{
    private final ZLog logger;
    private PreparedStatement phStmt;

    private static final String DB_OK = "DataBase connection OK";
    private static final String DB_FAILED = "DataBase connection failed";
    private static final String GET_ORGANIZATION =
        "Select t1.TerminalNumber, t1.Username, t1.Password, t2.Url, t2.Name " +
        "from clearingdb.organizations t1 join clearingdb.clearingcompanies t2 " +
        "on t1.Name=? and t1.Status=1 and t1.ClearingCompany = t2.Name;";
    private static final String INSERT_TRANSACTION =
        "INSERT INTO clearingdb.transactions (OrganizationName,VehicleNumber,DriverID,RefNum,TransactionDateTime,ManOrSwipe, " +
        "OperationType,CardLastDigits,CardBranch,Sum,Payments,AutorizationRef,StatusCode,ClientName,ClientPhone, " +
        "ClientAddress,ClientCity,ClientZip,ClientInfo,ClientID,ClientCompanyID,ProductInfo,Remarks,ParamX) " +
        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    
    public DataBaseEx() {
        super(CSMain.getSettings());
        logger = CSMain.getLogger();
    }

    public boolean init() {
        if (connect()) {
            logger.logEvent(DB_OK);
            return true;
        } else {
            logger.logEvent(DB_FAILED);
            return false;
        }
    }
    
    public TerminalInfo getOrganizationInfo(String orgName) {
        String terminalNumber = null, username = null, password = null, url = null, clearingService = null;
        try (PreparedStatement stmt = prepareStatement(GET_ORGANIZATION)) {
            stmt.setString(1, orgName);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    int i = 1;
                    terminalNumber = rs.getString(i++);
                    username = rs.getString(i++);
                    password = rs.getString(i++);
                    url = rs.getString(i++);
                    clearingService = rs.getString(i++);
                }
            }
        }   catch (SQLException ex) {
            logger.logException(ex);
        }

        return new TerminalInfo(clearingService, terminalNumber, username, password, url);
    }

    public void storeTransaction(Transaction transaction) {
        try (PreparedStatement stmt = prepareStatement(INSERT_TRANSACTION)) {
            transaction.fillStatement(stmt);
            execute(stmt);
        }   catch (SQLException ex) {
            logger.logException(ex);
        }
    }
}
