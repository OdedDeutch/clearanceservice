/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceService;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum CreditResponseType {
    Success(1), 
    ExpirationDateError(2),
    NoResonseForProvider(3),
    InsufficientFunds(4),
    StolenCard(5),
    CardNotSupported(6),
    InstalmentPaymentNotAllowsWithCard(7),
    Other(8);
    
    final private int type;
    
    private CreditResponseType(int type) {
        this.type = type;
    }
    
    /**
     * Get the numeric value of the enum according to Pelecard definition.
     * @return An integer with the value.
     */
    public int getValue() {
        return type;
    }
    
    public static CreditResponseType convert(String responsCode) {
        switch(responsCode) {
            case "000":
                return CreditResponseType.Success;
                
            case "002":
            case "005":
            case "074":
                return  CreditResponseType.StolenCard;
                 
            case "001":
            case "003":    
            case "004":
            case "041":
            case "042":
            case "101":
                return  CreditResponseType.InsufficientFunds;
            
            case "014":
            case "033":
            case "034":
                return  CreditResponseType.CardNotSupported;
                
            case "035":
                return  CreditResponseType.InstalmentPaymentNotAllowsWithCard;    
                
            case "036":
            case "508":
                return  CreditResponseType.ExpirationDateError;
 
            default:
                return CreditResponseType.Other;
        }
    }
}
