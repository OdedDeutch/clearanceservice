/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.clearanceService;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class Request {
    final private String token;
    final private String requestNumber;
    
    public Request(String token, String requestNumber) {
        this.token = token;
        this.requestNumber = requestNumber;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public String getToken() {
        return token;
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(3, 31)
                .append(requestNumber)
                .toHashCode();
    }
    
    @Override 
    public boolean equals(Object obj) {
        if (!(obj instanceof Request))
            return false;
        if (obj == this)
            return true;
        
        Request request = (Request) obj;
        return new EqualsBuilder()
                .append(requestNumber, request.requestNumber)
                .isEquals();     
    }
            
}
